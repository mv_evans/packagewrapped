#' This function searches in a defined directory and purls out all R code from Rmd documents
#'
#' @param path path to the directory you would like to search.
#' @param temporary.path where to create new temporary directory of R code from Rmd documents
#'
#' @import magrittr
#'
#' @export

Rmd_to_R <- function(path, temporary.path){

  library(magrittr)

  if (!dir.exists(file.path(temporary.path, "package-wrap-purl"))) {
    dir.create(file.path(temporary.path, "package-wrap-purl"))
    print("created package-wrap-purl directory")
  } else {
    print("saving in existing package-wrap-purl directory")
  }

  #find all Rmd files
  files <- c(list.files(path, recursive = T, pattern = "Rmd$", full.names = T))
  #limit to 2019

  #get date info and filter to year of interest
  rmd.files <- data.frame(
    files = files,
    date_mod= file.mtime(files)) %>%
    dplyr::mutate(date_mod = as.Date(date_mod)) %>%
    dplyr::mutate(year = lubridate::year(date_mod)) %>%
    dplyr::filter(year == 2019) %>%
    dplyr::mutate(output = paste0(temporary.path, "/package-wrap-purl/", date_mod, "_", dplyr::row_number(), ".R")) %>%
    dplyr::mutate(input = as.character(files))

  #purl each file and save in the package-wrap-purl directory
  purl_func <- purrr::possibly(knitr::purl, otherwise = print("error converting Rmd to R. moving to next file"))
  purrr::walk2(rmd.files$input, rmd.files$output, purl_func)

} #close Rmd_to_R function
