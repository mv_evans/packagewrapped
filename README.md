
# packageWrapped

PackageWrapped creates a 'spotify-style' 2019 Wrapped image with summary statistics on your most used R functions and packages of 2019.

![](https://bitbucket.org/mv_evans/packagewrapped/raw/06dcfd3e4ee69a46f637e0ebf8b85c68657a591d/examples/r-wrapped.png)

## Installation

You can install packageWrapped from bitbucket with:

```
devtools::install_bitbucket("mv_evans/packageWrapped")
```

You will need to have the [Asimov Regular font](https://www.fontspace.com/kineticplasma-fonts/asimov) installed on your machine.

## Creating your own Package Wrapped 2019 Image

There are three steps to creating your Packages Wrapped 2019 image:

1. extract R code from Rmd documents using `purl` (`Rmd_to_R` function)
2. generate a list of all functions and packages in the selected path (`check_packs` function)
3. create and save the image (`plot_wrapped` function)

The way this pacakge works is by searching through a specified directory on your computer for any R scripts. It then searches for any functions and identifies their packages. From this, you can calculate the frequencies of functions and packages used, and create the finally themed image.

A full example of this can be found in the `examples`.

### Extracting R code

You will need to choose the topmost directly that holds your code and where you would like to save a folder of all the R code that is extracted from Rmd files. This step will only need to be run once, if you keep the folder holding all of the extracted R code `package-wrap-purl`. For example, if I wanted to search all of my Dropbox, and save the extracted R code to the desktop, I would run the following:

**Mac OSX**

```
Rmd_to_R(path = "/Users/mvevans/Dropbox", temporary.path = "/Users/mvevans/Desktop")
```

This could take a while depending on how many Rmd files you need to purl. In my experience, it takes about 5 minutes per 100 files (although this probably depends on how long each file is).

### Package counts and statistics

All packages must be loaded in order to identify each function's appropriate package. This is done in the function, and can be turned off if packages are already loaded, or you want to manually choose which to load.

```
my_packs <- check_packs(path =  "/Users/mvevans/Dropbox",
                        purled_R_dir = "/Users/mvevans/Desktop/package-wrap-purl",
                        ignore.Rmd = T,
                        load.packages = T,
                        include.default = F)
```

The `path` argument should be the highest level of what you would like to search for R code. The `purled_R_dir` is the directory of extracted R code created in the first step. If you have purled the Rmd already, you can set `ignore.Rmd` to `TRUE`. If it is false, the function will still work, but will not count functions from Rmd files, and will return the number of Rmd files omitted. 

This step will return a list containing all the information needed to create the final image.

This is the most time-intensive step. It is recommended to save this object so you don't have to run it again.

### Create the image

Ensure the [Asimov Regular font](https://www.fontspace.com/kineticplasma-fonts/asimov) is installed on your machine. This will take the output of step two and save the image in the provided location.

```
plot_wrapped(my_packs, image.path = "/Users/mvevans/Desktop/r-2019-packages2.png")
```

# Input and Feedback

This package is very much a work in progress and especially needs input as far as adding additional packages hex stickers and categorizing packages to claculate the final *Top Genre*. If a hex sticker or package is missing, feel free to add them via a PR, or contact me at mvevans[at]uga.edu.

You can check out this [blog post](https://ditheringdata.netlify.com/2019/12/20/package-wrapped/) for more info about the inner workings of the package.